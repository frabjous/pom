#!/usr/bin/php
<?php

echo "Creating Web/HTML version...";
$html_source = shell_exec('php pom.php');
$html_source = mb_ereg_replace('⁰','<sup>0</sup>',$html_source);
$html_source = mb_ereg_replace('¹','<sup>1</sup>',$html_source);
$html_source = mb_ereg_replace('²','<sup>2</sup>',$html_source);
$html_source = mb_ereg_replace('³','<sup>3</sup>',$html_source);
$html_source = mb_ereg_replace('⁴','<sup>4</sup>',$html_source);
$html_source = mb_ereg_replace('⁵','<sup>5</sup>',$html_source);
$html_source = mb_ereg_replace('⁶','<sup>6</sup>',$html_source);
$html_source = mb_ereg_replace('⁷','<sup>7</sup>',$html_source);
$html_source = mb_ereg_replace('⁸','<sup>8</sup>',$html_source);
$html_source = mb_ereg_replace('⁹','<sup>9</sup>',$html_source);
$html_source = mb_ereg_replace('₀','<sub>0</sub>',$html_source);
$html_source = mb_ereg_replace('₁','<sub>1</sub>',$html_source);
$html_source = mb_ereg_replace('₂','<sub>2</sub>',$html_source);
$html_source = mb_ereg_replace('₃','<sub>3</sub>',$html_source);
$html_source = mb_ereg_replace('₄','<sub>4</sub>',$html_source);
$html_source = mb_ereg_replace('₅','<sub>5</sub>',$html_source);
$html_source = mb_ereg_replace('₆','<sub>6</sub>',$html_source);
$html_source = mb_ereg_replace('₇','<sub>7</sub>',$html_source);
$html_source = mb_ereg_replace('₈','<sub>8</sub>',$html_source);
$html_source = mb_ereg_replace('₀','<sub>9</sub>',$html_source);
$html_source = mb_ereg_replace('ⁿ','<sup><var>n</var></sup>',$html_source);
$html_source = mb_ereg_replace('ⁱ','<sup><var>i</var></sup>',$html_source);
$html_source = mb_ereg_replace('ₐ','<sub><var>a</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₑ','<sub><var>e</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₒ','<sub><var>o</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₓ','<sub><var>x</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₕ','<sub><var>h</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₖ','<sub><var>k</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₗ','<sub><var>l</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₘ','<sub><var>m</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₙ','<sub><var>n</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₚ','<sub><var>p</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₛ','<sub><var>s</var></sub>',$html_source);
$html_source = mb_ereg_replace('ₜ','<sub><var>t</var></sub>',$html_source);
file_put_contents('pom.html', $html_source);
unset($html_source);
echo " done." . PHP_EOL;


echo "Creating two-sided LaTeX source...";
$latex_source = shell_exec('php pom.php latex');
$latex_source = mb_ereg_replace('\\\\\)ε\\\\\(', '\\)\\,ε\\,\\(', $latex_source );
$latex_source = mb_ereg_replace('\\\\\)\\\\\(', '', $latex_source );
file_put_contents('pom.tex', $latex_source);
echo " done." . PHP_EOL;

echo "Compiling two-sided LaTeX source...";
exec('xelatex -interaction=nonstopmode pom.tex',$o,$e);
if ($e != 0) {
   echo ' FAILED!' . PHP_EOL;
   exit(1);
}
echo " done." . PHP_EOL;

echo "Creating one-sided LaTeX source...";
$latex_source = shell_exec('php pom.php latex portrait');
$latex_source = mb_ereg_replace('\\\\\)ε\\\\\(', '\\)\\,ε\\,\\(', $latex_source );
$latex_source = mb_ereg_replace('\\\\\)\\\\\(', '', $latex_source );
file_put_contents('pom-portrait.tex', $latex_source);
echo " done." . PHP_EOL;

echo "Compiling one-sided LaTeX source...";
exec('xelatex -interaction=nonstopmode pom-portrait.tex',$o,$e);
unset($latex_source);
if ($e != 0) {
   echo ' FAILED!' . PHP_EOL;
   exit(1);
}
echo " done." . PHP_EOL;

echo "Creating ePub version...";
exec('grep -v "settings.js" pom.html > pom-epub.html',$o,$e);
if ($e != 0) {
   echo ' FAILED!' . PHP_EOL;
   exit(1);
}
exec('ebook-convert pom-epub.html pom.epub --cover cover.jpg --author-sort "Russell, Bertrand" --authors "Bertrand Russell" --title "The Principles of Mathematics" --title-sort "Principles of Mathematics, The"',$o,$e);
if ($e != 0) {
   echo ' FAILED!' . PHP_EOL;
   exit(1);
}
echo " done." . PHP_EOL;

echo "Creating plain text version...";
exec('ebook-convert pom-epub.html pom.txt',$o,$e);
if ($e != 0) {
   echo ' FAILED!' . PHP_EOL;
   exit(1);
}
echo " done." . PHP_EOL;

echo "Creating TeX source zip archive...";
$zip = new ZipArchive();
if ($zip->open('pom-texsource.zip', ZipArchive::CREATE)!==TRUE) {
    echo ' FAILED!' . PHP_EOL;
    exit(1);
}
$zip->addFile('pom.tex');
$zip->addFile('pom-portrait.tex');
$zip->close();
echo " done." . PHP_EOL;

echo "Creating HTML source zip archive...";
$zip = new ZipArchive();
if ($zip->open('pom-htmlsource.zip', ZipArchive::CREATE)!==TRUE) {
    echo ' FAILED!' . PHP_EOL;
    exit(1);
}
$zip->addFile('pom.html');
$zip->addFile('settings.js');
$zip->addFile('figs/fig-sec203.png');
$zip->addFile('figs/fig-sec203.svg');
$zip->addFile('figs/fig-sec222.png');
$zip->addFile('figs/fig-sec222.svg');
$zip->addFile('figs/fig-sec226.png');
$zip->addFile('figs/fig-sec226.svg');
$zip->close();
echo " done." . PHP_EOL;


exit(0);
?>
